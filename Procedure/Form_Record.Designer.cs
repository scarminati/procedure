﻿namespace Procedure
{
    partial class Form_Record
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Record));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_linkProcedura = new System.Windows.Forms.TextBox();
            this.brn_OK = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tb_descrizioneProcedura = new System.Windows.Forms.TextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_Home = new System.Windows.Forms.ToolStripButton();
            this.tsSep_1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_showProcedure = new System.Windows.Forms.ToolStripButton();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_linkProcedura);
            this.groupBox3.Location = new System.Drawing.Point(12, 264);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(483, 190);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Path Procedura";
            // 
            // tb_linkProcedura
            // 
            this.tb_linkProcedura.Location = new System.Drawing.Point(20, 26);
            this.tb_linkProcedura.Multiline = true;
            this.tb_linkProcedura.Name = "tb_linkProcedura";
            this.tb_linkProcedura.Size = new System.Drawing.Size(447, 150);
            this.tb_linkProcedura.TabIndex = 3;
            // 
            // brn_OK
            // 
            this.brn_OK.ImageKey = "ok.png";
            this.brn_OK.ImageList = this.imageList;
            this.brn_OK.Location = new System.Drawing.Point(216, 460);
            this.brn_OK.Name = "brn_OK";
            this.brn_OK.Size = new System.Drawing.Size(50, 50);
            this.brn_OK.TabIndex = 4;
            this.brn_OK.UseVisualStyleBackColor = true;
            this.brn_OK.Click += new System.EventHandler(this.brn_OK_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "save.png");
            this.imageList.Images.SetKeyName(1, "export.png");
            this.imageList.Images.SetKeyName(2, "ok.png");
            this.imageList.Images.SetKeyName(3, "PDF.png");
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tb_descrizioneProcedura);
            this.groupBox4.Location = new System.Drawing.Point(12, 68);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(483, 190);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Descrizione Procedura";
            // 
            // tb_descrizioneProcedura
            // 
            this.tb_descrizioneProcedura.Location = new System.Drawing.Point(20, 26);
            this.tb_descrizioneProcedura.Multiline = true;
            this.tb_descrizioneProcedura.Name = "tb_descrizioneProcedura";
            this.tb_descrizioneProcedura.Size = new System.Drawing.Size(447, 150);
            this.tb_descrizioneProcedura.TabIndex = 2;
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator7,
            this.btn_Home,
            this.tsSep_1,
            this.btn_showProcedure});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(501, 50);
            this.toolStrip2.TabIndex = 22;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_Home
            // 
            this.btn_Home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Home.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Home.Image = ((System.Drawing.Image)(resources.GetObject("btn_Home.Image")));
            this.btn_Home.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(51, 47);
            this.btn_Home.ToolTipText = "Vai a Home";
            this.btn_Home.Click += new System.EventHandler(this.btn_Home_Click);
            // 
            // tsSep_1
            // 
            this.tsSep_1.Name = "tsSep_1";
            this.tsSep_1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_showProcedure
            // 
            this.btn_showProcedure.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_showProcedure.Image = ((System.Drawing.Image)(resources.GetObject("btn_showProcedure.Image")));
            this.btn_showProcedure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_showProcedure.Name = "btn_showProcedure";
            this.btn_showProcedure.Size = new System.Drawing.Size(51, 47);
            this.btn_showProcedure.Text = "toolStripButton2";
            this.btn_showProcedure.ToolTipText = "Visualizza Procedura";
            this.btn_showProcedure.Click += new System.EventHandler(this.btn_showProcedure_Click);
            // 
            // Form_Record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 518);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.brn_OK);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_Record";
            this.Text = "Aggiungi Procedura";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tb_linkProcedura;
        private System.Windows.Forms.Button brn_OK;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tb_descrizioneProcedura;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btn_Home;
        private System.Windows.Forms.ToolStripSeparator tsSep_1;
        private System.Windows.Forms.ToolStripButton btn_showProcedure;
    }
}

