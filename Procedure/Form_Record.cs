﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace Procedure
{
    public partial class Form_Record : Form
    {
        private int indexRecordToEdit = -1;
        private Utente user;
        private String reparto;

        public Form_Record()
        {
            try
            {
                InitializeComponent();
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Record(Utente user, String reparto) : this()
        {
            try
            {
                this.user = user;
                this.reparto = reparto;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_Record(Utente user, int indexRecordToEdit, bool isCloned) : this()
        {
            try
            {
                this.user = user;
                this.indexRecordToEdit = indexRecordToEdit;
                fillForm();
                if (isCloned)
                    this.indexRecordToEdit = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillForm()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Procedure] WHERE [ID] = '" + indexRecordToEdit.ToString() + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    tb_descrizioneProcedura.Text = dt.Rows[0].Field<string>("Descrizione Procedura").ToString();
                    tb_linkProcedura.Text = dt.Rows[0].Field<string>("Link Procedura");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(ConfigurationManager.AppSettings["PATH_PROCEDURE"]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_showProcedure_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(tb_linkProcedura.Text.Equals("") ? ConfigurationManager.AppSettings["PATH_PROCEDURE"] : tb_linkProcedura.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void brn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (indexRecordToEdit.Equals(-1))
                    searchRecordInDB();
                else
                    updateRecordInDB();
                if (this.DialogResult.Equals(DialogResult.OK))
                    this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Procedure]  WHERE " +
                    "[Descrizione Procedura] = '" + tb_descrizioneProcedura.Text.ToString().Replace("'", "''") + "' AND " +
                    "[Link Procedura] = '" + tb_linkProcedura.Text.ToString().Replace("'", "''") + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("La procedura è già esistente.\nSei sicuro di volerla sovrascrivere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                Utility.executeQuery("INSERT INTO [Procedure] " +
                "([Descrizione Procedura],[Link Procedura],[Data Aggiornamento],[Aggiornato Da]) " +
                "VALUES (" +
                    "'" + tb_descrizioneProcedura.Text.ToString().Replace("'","''") + "'," +
                    "'" + tb_linkProcedura.Text.Replace("'", "''") + "'," +
                    "'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + user.Nome + " " + user.Cognome + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [Procedure] SET " +
                "[Descrizione Procedura] = '" + tb_descrizioneProcedura.Text.ToString().Replace("'", "''") + "'," +
                "[Link Procedura] = '" + tb_linkProcedura.Text.Replace("'", "''") + "'," +
                "[Data Aggiornamento] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Aggiornato Da] = '" + user.Nome + " " + user.Cognome + "' " +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
