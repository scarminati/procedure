﻿namespace Procedure
{
    partial class Form_Main
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lbl_NomeUtente = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddMagazzino = new System.Windows.Forms.ToolStripButton();
            this.btn_CloneMagazzino = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_ColorLines = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_RemoveMagazzino = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_ExportMagazzino = new System.Windows.Forms.ToolStripButton();
            this.btn_PrintTestFiltro = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_Updates = new System.Windows.Forms.ToolStripButton();
            this.dgv_procedure = new System.Windows.Forms.DataGridView();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchProcedure = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_addProcedura = new System.Windows.Forms.ToolStripButton();
            this.tsSep_1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_delProcedura = new System.Windows.Forms.ToolStripButton();
            this.tsSep_2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportListaProcedure = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_procedure)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "boards.png");
            this.imageList.Images.SetKeyName(1, "clients.png");
            this.imageList.Images.SetKeyName(2, "repairs.png");
            this.imageList.Images.SetKeyName(3, "settings.png");
            this.imageList.Images.SetKeyName(4, "information.png");
            this.imageList.Images.SetKeyName(5, "technicalDrawing.png");
            this.imageList.Images.SetKeyName(6, "software.png");
            this.imageList.Images.SetKeyName(7, "Procedure.png");
            this.imageList.Images.SetKeyName(8, "qualityAssurance.png");
            this.imageList.Images.SetKeyName(9, "taskCompleted.png");
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // lbl_NomeUtente
            // 
            this.lbl_NomeUtente.Name = "lbl_NomeUtente";
            this.lbl_NomeUtente.Size = new System.Drawing.Size(78, 47);
            this.lbl_NomeUtente.Text = "Nome Utente";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddMagazzino
            // 
            this.btn_AddMagazzino.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_AddMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddMagazzino.Image")));
            this.btn_AddMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddMagazzino.Name = "btn_AddMagazzino";
            this.btn_AddMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_AddMagazzino.ToolTipText = "Aggiungi A Magazzino";
            this.btn_AddMagazzino.Visible = false;
            // 
            // btn_CloneMagazzino
            // 
            this.btn_CloneMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CloneMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_CloneMagazzino.Image")));
            this.btn_CloneMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CloneMagazzino.Name = "btn_CloneMagazzino";
            this.btn_CloneMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_CloneMagazzino.Text = "Clona Record";
            this.btn_CloneMagazzino.Visible = false;
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator10.Visible = false;
            // 
            // btn_ColorLines
            // 
            this.btn_ColorLines.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ColorLines.Image = ((System.Drawing.Image)(resources.GetObject("btn_ColorLines.Image")));
            this.btn_ColorLines.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ColorLines.Name = "btn_ColorLines";
            this.btn_ColorLines.Size = new System.Drawing.Size(51, 47);
            this.btn_ColorLines.Text = "Evidenzia";
            this.btn_ColorLines.ToolTipText = "Evidenzia";
            this.btn_ColorLines.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator3.Visible = false;
            // 
            // btn_RemoveMagazzino
            // 
            this.btn_RemoveMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RemoveMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_RemoveMagazzino.Image")));
            this.btn_RemoveMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RemoveMagazzino.Name = "btn_RemoveMagazzino";
            this.btn_RemoveMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_RemoveMagazzino.Text = "toolStripButton2";
            this.btn_RemoveMagazzino.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_RemoveMagazzino.Visible = false;
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator11.Visible = false;
            // 
            // btn_ExportMagazzino
            // 
            this.btn_ExportMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ExportMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_ExportMagazzino.Image")));
            this.btn_ExportMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ExportMagazzino.Name = "btn_ExportMagazzino";
            this.btn_ExportMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_ExportMagazzino.Text = "toolStripButton4";
            this.btn_ExportMagazzino.ToolTipText = "Esporta Magazzino";
            this.btn_ExportMagazzino.Visible = false;
            // 
            // btn_PrintTestFiltro
            // 
            this.btn_PrintTestFiltro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintTestFiltro.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintTestFiltro.Image")));
            this.btn_PrintTestFiltro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintTestFiltro.Name = "btn_PrintTestFiltro";
            this.btn_PrintTestFiltro.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintTestFiltro.ToolTipText = "Stampa Test";
            this.btn_PrintTestFiltro.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.lbl_NomeUtente,
            this.toolStripSeparator1,
            this.btn_Updates,
            this.btn_AddMagazzino,
            this.btn_CloneMagazzino,
            this.toolStripSeparator10,
            this.btn_ColorLines,
            this.toolStripSeparator3,
            this.btn_RemoveMagazzino,
            this.toolStripSeparator11,
            this.btn_ExportMagazzino,
            this.btn_PrintTestFiltro});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1242, 50);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_Updates
            // 
            this.btn_Updates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Updates.Image = ((System.Drawing.Image)(resources.GetObject("btn_Updates.Image")));
            this.btn_Updates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Updates.Name = "btn_Updates";
            this.btn_Updates.Size = new System.Drawing.Size(51, 47);
            this.btn_Updates.Text = "toolStripButton7";
            this.btn_Updates.ToolTipText = "Aggiorna Software";
            this.btn_Updates.Visible = false;
            this.btn_Updates.Click += new System.EventHandler(this.btn_Updates_Click);
            // 
            // dgv_procedure
            // 
            this.dgv_procedure.AllowUserToAddRows = false;
            this.dgv_procedure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_procedure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_procedure.Location = new System.Drawing.Point(0, 100);
            this.dgv_procedure.Name = "dgv_procedure";
            this.dgv_procedure.ReadOnly = true;
            this.dgv_procedure.Size = new System.Drawing.Size(1242, 412);
            this.dgv_procedure.TabIndex = 11;
            this.dgv_procedure.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_procedure_CellClick);
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator7,
            this.tstb_SearchProcedure,
            this.toolStripSeparator9,
            this.btn_addProcedura,
            this.tsSep_1,
            this.btn_delProcedura,
            this.tsSep_2,
            this.btn_exportListaProcedure,
            this.toolStripButton6});
            this.toolStrip2.Location = new System.Drawing.Point(0, 50);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1242, 50);
            this.toolStrip2.TabIndex = 10;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchProcedure
            // 
            this.tstb_SearchProcedure.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchProcedure.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchProcedure.Name = "tstb_SearchProcedure";
            this.tstb_SearchProcedure.Size = new System.Drawing.Size(800, 50);
            this.tstb_SearchProcedure.Text = "Cerca...";
            this.tstb_SearchProcedure.ToolTipText = "Cerca Procedura";
            this.tstb_SearchProcedure.Leave += new System.EventHandler(this.tstb_SearchProcedure_Leave);
            this.tstb_SearchProcedure.Click += new System.EventHandler(this.tstb_SearchProcedure_Click);
            this.tstb_SearchProcedure.TextChanged += new System.EventHandler(this.tstb_SearchProcedure_TextChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_addProcedura
            // 
            this.btn_addProcedura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addProcedura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addProcedura.Image = ((System.Drawing.Image)(resources.GetObject("btn_addProcedura.Image")));
            this.btn_addProcedura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addProcedura.Name = "btn_addProcedura";
            this.btn_addProcedura.Size = new System.Drawing.Size(51, 47);
            this.btn_addProcedura.ToolTipText = "Aggiungi Procedura";
            this.btn_addProcedura.Click += new System.EventHandler(this.btn_addProcedura_Click);
            // 
            // tsSep_1
            // 
            this.tsSep_1.Name = "tsSep_1";
            this.tsSep_1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_delProcedura
            // 
            this.btn_delProcedura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delProcedura.Image = ((System.Drawing.Image)(resources.GetObject("btn_delProcedura.Image")));
            this.btn_delProcedura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delProcedura.Name = "btn_delProcedura";
            this.btn_delProcedura.Size = new System.Drawing.Size(51, 47);
            this.btn_delProcedura.Text = "toolStripButton2";
            this.btn_delProcedura.ToolTipText = "Rimuovi Procedura";
            this.btn_delProcedura.Click += new System.EventHandler(this.btn_delProcedura_Click);
            // 
            // tsSep_2
            // 
            this.tsSep_2.Name = "tsSep_2";
            this.tsSep_2.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportListaProcedure
            // 
            this.btn_exportListaProcedure.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportListaProcedure.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportListaProcedure.Image")));
            this.btn_exportListaProcedure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportListaProcedure.Name = "btn_exportListaProcedure";
            this.btn_exportListaProcedure.Size = new System.Drawing.Size(51, 47);
            this.btn_exportListaProcedure.Text = "toolStripButton4";
            this.btn_exportListaProcedure.ToolTipText = "Esporta Lista Procedure";
            this.btn_exportListaProcedure.Click += new System.EventHandler(this.btn_exportListaProcedure_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton6.ToolTipText = "Stampa Test";
            this.toolStripButton6.Visible = false;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 512);
            this.Controls.Add(this.dgv_procedure);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "Procedure";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Main_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_procedure)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel lbl_NomeUtente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btn_AddMagazzino;
        private System.Windows.Forms.ToolStripButton btn_CloneMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton btn_ColorLines;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btn_RemoveMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton btn_ExportMagazzino;
        private System.Windows.Forms.ToolStripButton btn_PrintTestFiltro;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripButton btn_Updates;
        private System.Windows.Forms.DataGridView dgv_procedure;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btn_addProcedura;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator tsSep_1;
        private System.Windows.Forms.ToolStripButton btn_delProcedura;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchProcedure;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton btn_exportListaProcedure;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator tsSep_2;
    }
}

