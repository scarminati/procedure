﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procedure
{
    public class Utente
    {
        public int IdUtente { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Password { get; set; }
        public string Credenziale { get; set; }
        public string Location { get; set; }

        /// <summary>
        /// costruttore senza parametri
        /// </summary>
        public Utente()
        {
            //do nothing...
        }

        public Utente(int idUtente, string nome, string cognome, string password, string credenziale, string location)
        {
            IdUtente = idUtente;
            Nome = nome;
            Cognome = cognome;
            Password = password;
            Credenziale = credenziale;
            Location = location;
        }

    }
}
