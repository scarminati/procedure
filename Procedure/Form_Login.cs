﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procedure
{
    public partial class Form_Login : Form
    {
        private List<Utente> users;
        public Form_Login()
        {
            InitializeComponent();
            users = new List<Utente>();
            comboboxInitialization();
        }

        /// <summary>
        /// Inizializza le combobox del form
        /// </summary>
        private void comboboxInitialization()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [AutoTest].[dbo].[Utente2] WHERE [idUtente] IN (Select [idUtente] FROM [Autotest].[dbo].[Applicativo] WHERE [Autotest].[dbo].[Applicativo].[nomeApplicativo] = 'Procedure')");
                foreach (DataRow row in dt.Rows)
                    users.Add(new Utente(Convert.ToInt32(row["idUtente"]), row["nome"].ToString(), row["cognome"].ToString(), row["password"].ToString(), row["credenziale"].ToString(), row["location"].ToString()));
                foreach (var u in users)
                    cmb_UserName.Items.Add(u.Nome + " " + u.Cognome);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_UserName.SelectedItem == null)
                    MessageBox.Show("Inserisci il nome utente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    foreach (var usr in users)
                    {
                        if (cmb_UserName.SelectedItem.ToString().Equals(usr.Nome + " " + usr.Cognome))//idApplicativo = 4 => Controlli Centratura
                        {
                            if (txt_password.Text.Equals(usr.Password))
                            {
                                Form_Main form = new Form_Main(usr);
                                form.Show();
                                this.Hide();
                            }
                            else
                                MessageBox.Show("Password errata", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }                    
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
