﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing;
using System.IO;
using Xceed.Words.NET;
using System.Data.Sql;

namespace Procedure
{
    public static class Utility
    {
        /// <summary>
        /// esegue una query sul DB e restituisce i dati sotto forma di DataTable
        /// </summary>
        /// <param name="sqlCommand">query da eseguire</param>
        /// <returns></returns>
        public static DataTable getDataTableFromDB(string sqlCommand)
        {
            try
            {
                string connectionString = "Data Source = RALCOSRV4\\SQLEXPRESS;Initial Catalog = Autotest; User ID = sitem; Password=sitem";
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlDataAdapter adapt = new SqlDataAdapter(sqlCommand, connection);
                connection.Close();
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public static int executeQuery(string sqlCommand)
        {
            try
            {
                int retValue = 0;
                string connectionString = "Data Source = RALCOSRV4\\SQLEXPRESS;Initial Catalog = Autotest; User ID = sitem; Password=sitem";
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand command = new SqlCommand(sqlCommand, connection);
                retValue = command.ExecuteNonQuery();
                connection.Close();
                return (retValue);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Esegue una query su tutti i campi della tabella
        /// </summary>
        /// <param name="tableName">tabella su cui eseguire la query</param>
        /// <param name="dataToSearch">dato da cercare in ogni record della tabella</param>
        /// <returns></returns>
        public static string getSQLCondition_ForeachColumn(string tableName, string dataToSearch, params string[] externCondition)
        {
            try
            {
                string selectCondition = "";
                DataTable dt = getDataTableFromDB("SELECT TOP(1) * FROM [" + tableName + "]");
                foreach (DataColumn col in dt.Columns)
                {
                    if (selectCondition.Equals(""))
                        selectCondition = " WHERE " + (externCondition == null ? "" : "(" + externCondition[0] + ") AND (") + "[" + col + "] LIKE '%" + dataToSearch + "%'";
                    else
                        selectCondition = selectCondition + " OR [" + col + "] LIKE '%" + dataToSearch + "%'";
                }
                return selectCondition + (externCondition == null ? "" : ")");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DataTable aggiornaDataGridView(string sqlCommand)
        {
            try
            {
                return(Utility.getDataTableFromDB(sqlCommand));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public static void saveAndVisualizeExcelReport(DataGridView dgv, string workSheetName)
        {
            try
            {
                ExcelPackage excel = new ExcelPackage();
                //Add the worksheet Report
                excel.Workbook.Worksheets.Add(workSheetName);
                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(dgv.ColumnCount + 64) + "1";
                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets[workSheetName];
                // Popular header row data
                DataTable dataTable = (DataTable)(dgv.DataSource);
                worksheet.Cells[headerRange].LoadFromDataTable(dataTable, true);
                //set cells layout
                worksheet.Cells[1, 1, 1, dgv.ColumnCount].Style.Font.Bold = true;
                worksheet.Cells[1, 1, dgv.RowCount, dgv.ColumnCount].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells[1, 1, dgv.RowCount, dgv.ColumnCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 1, dgv.RowCount, dgv.ColumnCount].AutoFitColumns();
                worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                worksheet.PrinterSettings.FitToPage = true;
                //save the excel file report
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\");
                FileInfo excelFile = new FileInfo(Environment.CurrentDirectory + @"\Reports\Extract_" + workSheetName + ".xlsx");
                excel.SaveAs(excelFile);
                System.Diagnostics.Process.Start(Environment.CurrentDirectory + @"\Reports\Extract_" + workSheetName + ".xlsx");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void saveAndVisualizeWordReport(DataGridView dgv, int index)
        {
            try
            {
                string pathTemplate = Environment.CurrentDirectory + @"\Templates\Report_Template.docx";
                var doc = DocX.Load(pathTemplate);
                //salvataggio del file
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\");
                string pathReport = Environment.CurrentDirectory + @"\Reports\Report_" + DateTime.Now.ToString().Replace('/','_').Replace(':', '_').Replace(' ', 'h') + ".docx";
                DataTable dt_Settings = Utility.getDataTableFromDB("SELECT * FROM [Settings]");
                doc.ReplaceText("@@RS_TTL@@","Ragione Sociale");
                doc.ReplaceText("@@RS_DSC@@", dt_Settings.Rows[0].Field<string>("Ragione Sociale"));
                doc.SaveAs(pathReport);
                System.Diagnostics.Process.Start(pathReport);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Verifica la presenza di aggiornamenti
        /// </summary>
        public static bool checkForUpdates()
        {
            try
            {
                removeBakFiles();
                string pathServer = ConfigurationManager.AppSettings["SERVER_UPDATE_APP_PATH"];
                if (pathServer != null)
                {
                    string[] dirs = Directory.GetFiles(pathServer, "Procedure.exe");
                    if (dirs.Length.Equals(1))
                    {
                        FileInfo serverFileInfo = new FileInfo(dirs[0]);
                        FileInfo appFileInfo = new FileInfo(Environment.CurrentDirectory + @"\Procedure.exe");
                        if (serverFileInfo.LastWriteTime > appFileInfo.LastWriteTime)
                            return (true);
                        else
                            return false;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static void removeBakFiles()
        {
            try
            {
                string[] dirs = Directory.GetFiles(Environment.CurrentDirectory, "Procedure.bak");
                if (!dirs.Length.Equals(0))
                {
                    File.Delete(dirs[0]);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
