﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace Procedure
{
    public partial class Form_Main : Form
    {
        private string selectCondition = "";
        private Utente user;
        private bool adminPrivileges;
        private bool changeColorLines = false;
        private Color colorLines = Color.White;
        public Form_Main(Utente user)
        {
            try
            {
                InitializeComponent();
                this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                setApplicationVersion();
                this.user = user;
                lbl_NomeUtente.Text = user.Nome + " " + user.Cognome;
                tabControlList_SelectedIndexChanged(null, null);
                if (ConfigurationManager.AppSettings["LOCAL_TEST"].Equals("false"))
                {
                    bool updatesAvailable = Utility.checkForUpdates();
                    if (updatesAvailable)
                    {
                        btn_Updates.Visible = true;
                    }
                }
                setPrivilegesLayout();
                setDataGridViewLayout();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tabControlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDataGridViewLayout();
        }

        private void Form_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        #region MetodiGenerali

        private void setApplicationVersion()
        {
            // considero solo i primi 2 valori per la versione da visualizzare
            String tmp = Application.ProductVersion;
            int pos = tmp.IndexOf(".");
            pos = tmp.IndexOf(".", pos + 1);

            this.Text += " - V. " + tmp.Substring(0, pos);
        }

        /// <summary>
        /// Ripristina il valore di default della textbox di ricerca e tutte le datagridview
        /// </summary>
        /// <param name="tb"></param>
        private void resetSearchTextboxAndDatagridviews(ToolStripTextBox tb)
        {
            if (tstb_SearchProcedure.Text.Equals(""))
            {
                tstb_SearchProcedure.Text = "Cerca...";
            }
            //aggiungere le atre ricerche QUI...
            tabControlList_SelectedIndexChanged(null, null);
            setDataGridViewLayout();
        }

        /// <summary>
        /// modifica la visualizzazione dei dati nella datagridview in accordo con la selezione scritta nella corrispondente textbox di ricerca
        /// </summary>
        /// <param name="tb"></param>
        private void changeDataDisplayed(ToolStripTextBox tb)
        {
            try
            {
                if (!tstb_SearchProcedure.Text.Equals("") && !tstb_SearchProcedure.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("Procedure", tstb_SearchProcedure.Text, null);
                    dgv_procedure.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Procedure] " + selectCondition);
                }
                //aggiungere le atre ricerche QUI...
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setDataGridViewLayout()
        {
            try
            {

                dgv_procedure.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Procedure]");
                dgv_procedure.Columns["ID"].Visible = false;
                dgv_procedure.Columns["Link Procedura"].Visible = false;
                dgv_procedure.Columns["Descrizione Procedura"].Width = 900;
                //dgv_procedure.Columns["Data Aggiornamento"].Visible = false;
                //dgv_procedure.Columns["Task"].Width = 600;
                //colorDataGridViewRows(dgv_procedure);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void colorDataGridViewRows(DataGridView dgv)
        {
            //try
            //{
            //    foreach (DataGridViewRow r in dgv.Rows)
            //    {
            //        if (r.Cells["Priorità"].Value.ToString().Equals("3 - Top"))
            //            r.DefaultCellStyle.BackColor = Color.Salmon;
            //        else if (r.Cells["Priorità"].Value.ToString().Equals("2 - Alta"))
            //            r.DefaultCellStyle.BackColor = Color.Orange;
            //        else if (r.Cells["Priorità"].Value.ToString().Equals("1 - Media"))
            //            r.DefaultCellStyle.BackColor = Color.LightGreen;
            //        else if (r.Cells["Priorità"].Value.ToString().Equals("0 - Bassa"))
            //            r.DefaultCellStyle.BackColor = Color.White;
            //    }
            //    dgv.DefaultCellStyle.Font = new Font("Arial", 12.0F, GraphicsUnit.Pixel);
            //    dgv.Refresh();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void setPrivilegesLayout()
        {
            if (!user.Credenziale.Equals("admin"))
            {
                adminPrivileges = false;
            }
            else
                adminPrivileges = true;
            btn_addProcedura.Visible = adminPrivileges;
            tsSep_1.Visible = adminPrivileges;
            btn_delProcedura.Visible = adminPrivileges;
            tsSep_2.Visible = adminPrivileges;
            btn_exportListaProcedure.Visible = adminPrivileges;
        }

        private void btn_Updates_Click(object sender, EventArgs e)
        {
            try
            {
                string pathServer = ConfigurationManager.AppSettings["SERVER_UPDATE_APP_PATH"];
                FileInfo serverFileInfo;
                FileInfo localFileInfo;
                string[] pathFiles = Directory.GetFiles(pathServer);
                if (pathServer != null)
                {
                    for (int i = 0; i < pathFiles.Length; i++)
                    {
                        string fileName = Path.GetFileName(pathFiles[i]);
                        serverFileInfo = new FileInfo(pathFiles[i]);
                        localFileInfo = new FileInfo(Environment.CurrentDirectory + @"\" + fileName);
                        if (!fileName.Equals("Procedure.exe"))
                        {
                            if (serverFileInfo.LastWriteTime > localFileInfo.LastWriteTime)
                                File.Copy(serverFileInfo.ToString(), localFileInfo.ToString(), true);
                        }
                        else
                        {
                            File.Move(Environment.CurrentDirectory + @"\Procedure.exe", Environment.CurrentDirectory + @"\Procedure.bak");
                            File.Copy(serverFileInfo.ToString(), localFileInfo.ToString(), true);
                        }
                    }
                    DialogResult result = MessageBox.Show("Il programma verrà riavviato per consentire l\'installazione degli aggiornamenti.\nPremere OK per continuare...", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (result.Equals(DialogResult.OK))
                    {
                        //File.Delete(Environment.CurrentDirectory + @"\Ralco_ServiceManager.bak");
                        this.Dispose();
                        Application.Restart();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region operazioniComuni

        private void addTask()
        {
            try
            {
                Form_Record formRip = new Form_Record(user, "");
                var dialogResult = formRip.ShowDialog();
                if (dialogResult.Equals(DialogResult.OK))
                    tabControlList_SelectedIndexChanged(null, null);
                setDataGridViewLayout();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void editTask(DataGridViewCellEventArgs e, DataGridView dgv)
        {
            try
            {
                if (e.RowIndex != -1 && dgv.Rows[dgv.CurrentCell.RowIndex].Cells["ID"].Value.ToString() != "" && !dgv.SelectedCells.Count.Equals(dgv.Rows[dgv.CurrentCell.RowIndex].Cells.Count))
                {
                    int indexRecordToEdit = Convert.ToInt32(dgv.Rows[dgv.CurrentCell.RowIndex].Cells["ID"].Value.ToString());
                    Form_Record formArt = new Form_Record(user, indexRecordToEdit, false);
                    var dialogResult = formArt.ShowDialog();
                    if (dialogResult.Equals(DialogResult.OK))
                        tabControlList_SelectedIndexChanged(null, null);
                    setDataGridViewLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void deleteTask(DataGridView dgv)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("L'elemento selezionato verrà rimosso dal database.\nSei sicuro di voler procedere?", "Elimina Riferimento Procedura", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    int actualIndex = dgv.CurrentCell.RowIndex;
                    Utility.executeQuery("DELETE FROM [Procedure] WHERE " +
                           "[Id] = '" + dgv.Rows[actualIndex].Cells["Id"].Value.ToString() + "'");
                    tstb_SearchProcedure.Text = "";
                    tabControlList_SelectedIndexChanged(null, null);
                    setDataGridViewLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportTask(DataGridView dgv)
        {
            try
            {
                Utility.saveAndVisualizeExcelReport(dgv, "Report Procedure");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Procedure

        private void dgv_procedure_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (adminPrivileges)
                    editTask(e, dgv_procedure);
                else
                {
                    DataTable dt = Utility.aggiornaDataGridView("SELECT * FROM [Procedure] WHERE [Descrizione Procedura] = '" + dgv_procedure.CurrentRow.Cells[1].Value.ToString().Replace("'", "''") + "'");
                    System.Diagnostics.Process.Start(dt.Rows.Count.Equals(0) ? ConfigurationManager.AppSettings["PATH_PROCEDURE"] : dt.Rows[0].Field<string>("Link Procedura"));
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tstb_SearchProcedure_Click(object sender, EventArgs e)
        {
            tstb_SearchProcedure.Text = "";
            tstb_SearchProcedure_TextChanged(null, null);
        }

        private void tstb_SearchProcedure_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchProcedure);
        }

        private void tstb_SearchProcedure_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchProcedure);
        }

        private void btn_addProcedura_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delProcedura_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_procedure);
        }

        private void btn_exportListaProcedure_Click(object sender, EventArgs e)
        {
            exportTask(dgv_procedure);
        }

        #endregion

    }
}
